package eu.uwot.fabio.altcoinprices;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Coin {

    final String[] coins = new String[] {
        "BTC",
        "BCH",
        "ETH",
        "LTC",

        "AE",
        "REP",
        "BAT",
        "BNB",
        "BCD",
        "BTG",
        "BTS",
        "BLK",
        "BCN",
        "BTM",
        "ADA",
        "LINK",
        "DASH",
        "DCR",
        "DGD",
        "DOGE",
        "EOS",
        "ETC",
        "GNO",
        "ICX",
        "ICN",
        "IOT",
        "LSK",
        "MLN",
        "XMR",
        "NANO",
        "XEM",
        "NEO",
        "OMG",
        "ONT",
        "PART",
        "PPT",
        "QTUM",
        "RHOC",
        "REQ",
        "XRP",
        "SC",
        "SNT",
        "STEEM",
        "XLM",
        "STRAT",
        "TRTL",
        "TRX",
        "VEN",
        "XVG",
        "WAVES",
        "ZEC",

        "BLX",
        "CCC",
        "SOPR",
        "FCI",
        "GEM",
        "PCC",
        "WMX",
        "BMC",
        "AAAX",
        "CAR",
        "KCOR",
        "CBST",
        "RCAA",
        "BIF",
        "CRNC",
        "BGA15",
        "BEA",
        "BLS",
        "FCE",
        "PPI",
        "JJK",
        "TRADE",
        "EAA",
        "SCX",
        "CCP"
    };
    private final String[] iconomi_daa = new String[]{
        "BLX",
        "CCC",
        "SOPR",
        "FCI",
        "GEM",
        "PCC",
        "WMX",
        "BMC",
        "AAAX",
        "CAR",
        "KCOR",
        "CBST",
        "RCAA",
        "BIF",
        "CRNC",
        "BGA15",
        "BEA",
        "BLS",
        "FCE",
        "PPI",
        "JJK",
        "TRADE",
        "EAA",
        "SCX",
        "CCP"
    };
    private final String[] descriptions = new String[]{
        "Bitcoin",
        "Bitcoin Cash",
        "Ethereum",
        "Litecoin",

        "Aeternity",
        "Augur",
        "Basic Attention Token",
        "Binance Coin",
        "Bitcoin Diamond",
        "Bitcoin Gold",
        "Bitshares",
        "BlackCoin",
        "Bytecoin",
        "Bytom",
        "Cardano",
        "ChainLink",
        "Dash",
        "Decred",
        "Digix DAO",
        "Dogecoin",
        "EOS",
        "Ethereum Classic",
        "Gnosis",
        "Icon",
        "Iconomi",
        "IOTA",
        "Lisk",
        "Melon",
        "Monero",
        "Nano",
        "NEM",
        "NEO",
        "OmiseGO",
        "Ontology",
        "Particl",
        "Populous",
        "Qtum",
        "RChain",
        "Request Network",
        "Ripple",
        "Siacoin",
        "Status Network Token",
        "Steem",
        "Stellar Lumens",
        "Stratis",
        "TurtleCoin",
        "TRON",
        "VeChain",
        "Verge",
        "Waves",
        "Zcash",

        "Blockchain Index (ICN DAA)",
        "Crush Crypto Core (ICN DAA)",
        "Solidum Prime (ICN DAA)",
        "Future Chain Index (ICN DAA)",
        "Greychain Emerging Markets (ICN DAA)",
        "The Pecunio Cryptocurrency (ICN DAA)",
        "William Mougayar High Growth Cryptoassets Index (ICN DAA)",
        "BMC Original (ICN DAA)",
        "The Asymmetry DAA (ICN DAA)",
        "CARUS-AR (ICN DAA)",
        "KryptoStar CORE (ICN DAA)",
        "Coinbest 1 (ICN DAA)",
        "Ragnarok Crypto Asset Array (ICN DAA)",
        "Blockchain Infrastructure Index (ICN DAA)",
        "Cornucopia Index (ICN DAA)",
        "Global Blockchain Arrays represents (ICN DAA)",
        "Blockchain Easy Access (ICN DAA)",
        "Blockchain Smart (ICN DAA)",
        "Future Crypto Economy (ICN DAA)",
        "Phoenix Paradigm Indicator (ICN DAA)",
        "JJK Crypto Assets (ICN DAA)",
        "Trade (ICN DAA)",
        "Exponential Age Array (ICN DAA)",
        "StrongCoindex (ICN DAA)",
        "Pinta (ICN DAA)"
    };
    String[] coinsLabelDescriptionsString;
    Hashtable<String, String> coinsLabelDescriptionHashtable;
    Hashtable<String, String> coinsDescriptionLabelHashtable;
    private final Hashtable<String, String> coinsLabelExchangeHashtable = new Hashtable<String, String>()
    {{  put("BTCUSD", "coinbase");
        put("ETHUSD", "coinbase");

        put("BTC", "coinbase");
        put("BCH", "coinbase");
        put("ETH", "coinbase");
        put("LTC", "coinbase");

        put("AE", "binance");
        put("REP", "bittrex");
        put("BAT", "bittrex");
        put("BNB", "binance");
        put("BCD", "binance");
        put("BTG", "bittrex");
        put("BTS", "bittrex");
        put("BLK", "bittrex");
        put("BCN", "hitbtc");
        put("BTM", "cryptocompare");
        put("ADA", "bittrex");
        put("LINK", "binance");
        put("DASH", "bittrex");
        put("DCR", "bittrex");
        put("DGD", "binance");
        put("DOGE", "bittrex");
        put("EOS", "bitfinex");
        put("ETC", "bittrex");
        put("GNO", "bittrex");
        put("ICX", "binance");
        put("ICN", "kraken");
        put("IOT", "bitfinex");
        put("LSK", "bittrex");
        put("MLN", "bittrex");
        put("XMR", "bittrex");
        put("NANO", "binance");
        put("XEM", "bittrex");
        put("NEO", "bittrex");
        put("OMG", "bittrex");
        put("ONT", "binance");
        put("PART", "bittrex");
        put("PPT", "binance");
        put("QTUM", "bittrex");
        put("RHOC", "cryptocompare");
        put("REQ", "binance");
        put("XRP", "bittrex");
        put("SC", "bittrex");
        put("SNT", "binance");
        put("STEEM", "bittrex");
        put("XLM", "bittrex");
        put("STRAT", "bittrex");
        put("TRTL", "cryptocompare");
        put("TRX", "bitfinex");
        put("VEN", "binance");
        put("XVG", "bittrex");
        put("WAVES", "binance");
        put("ZEC", "bittrex");

        put("BLX", "cryptocompare");
        put("CCC", "cryptocompare_icndaa");
        put("SOPR", "cryptocompare_icndaa");
        put("FCI", "cryptocompare_icndaa");
        put("GEM", "cryptocompare_icndaa");
        put("PCC", "cryptocompare_icndaa");
        put("WMX", "cryptocompare_icndaa");
        put("BMC", "cryptocompare_icndaa");
        put("AAAX", "cryptocompare_icndaa");
        put("CAR", "cryptocompare_icndaa");
        put("KCOR", "cryptocompare_icndaa");
        put("CBST", "cryptocompare_icndaa");
        put("RCAA", "cryptocompare_icndaa");
        put("BIF", "cryptocompare_icndaa");
        put("CRNC", "cryptocompare_icndaa");
        put("BGA15", "cryptocompare_icndaa");
        put("BEA", "cryptocompare_icndaa");
        put("BLS", "cryptocompare_icndaa");
        put("FCE", "cryptocompare_icndaa");
        put("PPI", "cryptocompare_icndaa");
        put("JJK", "cryptocompare_icndaa");
        put("TRADE", "cryptocompare_icndaa");
        put("EAA", "cryptocompare_icndaa");
        put("SCX", "cryptocompare_icndaa");
        put("CCP", "cryptocompare_icndaa");
    }};
    final Hashtable<String, String> coinsLabelGraph = new Hashtable<String, String>()
    {{  put("BTCUSD", "USD");
        put("ETHUSD", "USD");

        put("BTC", "free");
        put("BCH", "free");
        put("ETH", "free");
        put("LTC", "free");

        put("AE", "BTC");
        put("REP", "USD");
        put("BAT", "USD");
        put("BNB", "USDT");
        put("BCD", "USD");
        put("BTG", "USDT");
        put("BTS", "USDT");
        put("BLK", "USD");
        put("BCN", "USD");
        put("BTM", "na");
        put("ADA", "USDT");
        put("LINK", "USD");
        put("DASH", "USDT");
        put("DCR", "USD");
        put("DGD", "USD");
        put("DOGE", "USD");
        put("EOS", "USD");
        put("ETC", "USDT");
        put("GNO", "USD");
        put("ICX", "USD");
        put("ICN", "USD");
        put("IOT", "USD");
        put("LSK", "USD");
        put("MLN", "USD");
        put("XMR", "USDT");
        put("NANO", "BTC");
        put("XEM", "USD");
        put("NEO", "USDT");
        put("OMG", "USDT");
        put("ONT", "BTC");
        put("PART", "USD");
        put("PPT", "USD");
        put("QTUM", "USD");
        put("RHOC", "na");
        put("REQ", "USD");
        put("XRP", "USDT");
        put("SC", "USDT");
        put("SNT", "USD");
        put("STEEM", "USD");
        put("XLM", "USD");
        put("STRAT", "USD");
        put("TRX", "USD");
        put("TRTL", "na");
        put("VEN", "USD");
        put("XVG", "USDT");
        put("WAVES", "USD");
        put("ZEC", "USDT");

        put("BLX", "na");
        put("CCC", "na");
        put("SOPR", "na");
        put("FCI", "na");
        put("GEM", "na");
        put("PCC", "na");
        put("WMX", "na");
        put("BMC", "na");
        put("AAAX", "na");
        put("CAR", "na");
        put("KCOR", "na");
        put("CBST", "na");
        put("RCAA", "na");
        put("BIF", "na");
        put("CRNC", "na");
        put("BGA15", "na");
        put("BEA", "na");
        put("BLS", "na");
        put("FCE", "na");
        put("PPI", "na");
        put("JJK", "na");
        put("TRADE", "na");
        put("EAA", "na");
        put("SCX", "na");
        put("CCP", "na");
    }};

    private final Context context;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private float btcusd;
    private float btceur;
    private float usdeur;
    private float eurusd;

    public Coin(Context context, boolean doBTCUSDEUR) {
        this.context = context.getApplicationContext();
        if (doBTCUSDEUR == true) {
            prefs = context.getSharedPreferences("Settings", 0);
            btcusd = prefs.getFloat("btcusd", 1);
            btceur = prefs.getFloat("btceur", 1);
            usdeur = btceur / btcusd;
            eurusd = btcusd / btceur;
        }
        initCoins();
    }

    private void initCoins() {
        coinsLabelDescriptionsString = new String[coins.length];
        for (int i = 0; i < coins.length; i++) {
            coinsLabelDescriptionsString[i] = coins[i] + " - " + descriptions[i];
        }

        coinsLabelDescriptionHashtable = new Hashtable<>();
        for (int i = 0; i < coins.length; i++) {
            coinsLabelDescriptionHashtable.put(coins[i], descriptions[i]);
        }

        coinsDescriptionLabelHashtable = new Hashtable<>();
        coinsDescriptionLabelHashtable.put("BTC/USD - Bitcoin", "BTCUSD");
        coinsDescriptionLabelHashtable.put("ETH/USD - Ethereum", "ETHUSD");
        for (int i = 0; i < coins.length; i++) {
            coinsDescriptionLabelHashtable.put(coins[i] + " - " + descriptions[i], coins[i]);
        }
    }

    // Get which exchange trade a coin //
    public String getCoinExchange (String altcoinName) {
        return coinsLabelExchangeHashtable.get(altcoinName);
    }

    public boolean isItIconomiDaa (String altcoinName) {
        for (int i = 0; i < iconomi_daa.length; i++) {
            if (altcoinName.equals(iconomi_daa[i])) {
                return true;
            }
        }

        return false;
    }

    public boolean addItem(String altcoinDesc, float amountBought, float unitPrice, String currency) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0);
        editor = prefs.edit();

        String altcoinLabel = coinsDescriptionLabelHashtable.get(altcoinDesc);
        float amountBought_old = prefs.getFloat(altcoinLabel + "_a", -1f);

        if (amountBought_old != -1f) {
            String altcoinCurrency = prefs.getString(altcoinLabel + "_currency", "EUR");
            float unitPrice_old = prefs.getFloat(altcoinLabel + "_p", -1f);

            // Amount can't be a negative number
            if (amountBought_old + amountBought > 0) {
                // Convert value to the correct currency if needed
                if (!altcoinCurrency.equals(currency)) {
                    unitPrice = currencyToCurrency(unitPrice, altcoinCurrency, currency);
                }

                unitPrice = (amountBought_old * unitPrice_old + amountBought * unitPrice) / (amountBought_old + amountBought);
                amountBought = amountBought_old + amountBought;
            } else {
                Log.d("DEBUG", "Portfolio Item can't contain a zero/negative amount of coins");
                return false;
            }
        } else {
            editor.putString(altcoinLabel + "_currency", currency);
        }

        editor.putFloat(altcoinLabel + "_a", amountBought);
        editor.putFloat(altcoinLabel + "_p", unitPrice);
        editor.apply();

        return true;
    }

    public boolean editItem(String altcoinName, float amountBought, float unitPrice) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0);
        editor = prefs.edit();

        if ((amountBought >= 0) && (unitPrice >= 0)) {
            editor.putFloat(altcoinName + "_a", amountBought);
            editor.putFloat(altcoinName + "_p", unitPrice);
            editor.apply();
        } else {
            Log.d("DEBUG", "Portfolio Item can't contain a zero/negative amount of coins");
            return false;
        }

        return true;
    }

    public void removeItem(String altcoinName) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0);
        editor = prefs.edit();

        editor.remove(altcoinName + "_a");
        editor.remove(altcoinName + "_p");
        editor.remove(altcoinName + "_currentUnitValue");
        editor.remove(altcoinName + "_lastDayUnitValue");
        editor.remove(altcoinName + "_currency");
        editor.apply();
    }

    // Get current coin value from exchange //
    public float getCoinValue(String altcoinName, String currency, boolean historical) {
        float coinValue;

        if (!getCoinExchange(altcoinName).equals("cryptocompare_icndaa")) {
            coinValue = getCoinQuoteCryptoCompare(altcoinName, currency, historical);
        } else {
            coinValue = -1f;
        }

        // Exchange API are down or reporting broken values
        if (coinValue == -1f) {
            coinValue = getCoinInitialValue(altcoinName);
        }

        return coinValue;
    }

    // Get coin change in BTC from cryptocompare.com //
    // CURRENT    REQUEST: https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD
    // CURRENT    RESPONSE: {"USD":6945.12}
    // HISTORICAL REQUEST: https://min-api.cryptocompare.com/data/pricehistorical?fsym=BTC&tsyms=USD&ts=1524831212960
    // HISTORICAL RESPONSE: {"BTC":{"USD":9254.98}}
    private float getCoinQuoteCryptoCompare(String altcoinName, String currency, boolean historical) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        String dataSTR = "";
        float coinQuote = -1f;

        if (historical == false) {
            try {
                url = new URL("https://min-api.cryptocompare.com/data/price?fsym=" +
                        altcoinName +
                        "&tsyms=" +
                        currency);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                url = new URL("https://min-api.cryptocompare.com/data/pricehistorical?fsym=" +
                        altcoinName +
                        "&tsyms=" +
                        currency);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        try {
            assert url != null;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000); // 5 seconds timeout

            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                dataSTR = dataSTR + Character.toString(current);
            }
        } catch (SocketTimeoutException e) {
            dataSTR = "0";
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert urlConnection != null;
            urlConnection.disconnect();
        }

        Pattern pattern = Pattern.compile("([0-9]+).([0-9]+)");
        Matcher matcher = pattern.matcher(dataSTR);

        if (matcher.find()) {
            coinQuote = Float.parseFloat(matcher.group(0));
        }

        return coinQuote;
    }

    // Convert currencies //
    public float currencyToCurrency(float price, String currency, String newCurrency) {
        switch (currency) {
            case "USD":
                switch (newCurrency) {
                    case "EUR":
                        price *= usdeur;
                        break;
                    case "BTC":
                        price *= 1 / btcusd;
                        break;
                }
                break;
            case "EUR":
                switch (newCurrency) {
                    case "USD":
                        price *= eurusd;
                        break;
                    case "BTC":
                        price *= 1 / btceur;
                        break;
                }
                break;
            case "BTC":
                switch (newCurrency) {
                    case "USD":
                        price *= btcusd;
                        break;
                    case "EUR":
                        price *= btceur;
                        break;
                }
                break;
        }

        return price;
    }

    // Get the amount of FIAT a coin was paid //
    private float getCoinInitialValue(String altcoinName) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0);

        return prefs.getFloat(altcoinName + "_p", -1f);
    }

}
