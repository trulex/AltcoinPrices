package eu.uwot.fabio.altcoinprices;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String currency;
    private Coin coin;
    private boolean portfolioPeriodIsDay;
    private ArrayList<PortfolioItem> portfolioItems = new ArrayList<>();
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    // Activity's entry point, onCreateDo() is also used to refresh the Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        prefs = getApplicationContext().getSharedPreferences("Settings", 0);
        editor = prefs.edit();
        currency = prefs.getString("currency", "EUR");
        portfolioPeriodIsDay = prefs.getBoolean("portfolioPeriodIsDay", false);
    }

    // Reload portfolio data when coming back from nested Activity //
    @Override
    public void onResume() {
        super.onResume();
        portfolioItems = new ArrayList<>();
        onCreateDo();
    }

    // Populate portfolio, sidebar, etc
    private void onCreateDo() {
        final ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        coin = new Coin(getApplicationContext(), true);
        getPortfolioData();

        // add current balance in FIAT //
        TextView portfolioBalance = findViewById(R.id.portfolioBalance);
        //currency = prefs.getString("currency", "EUR");
        final float currentPortfolioValueFiat = getCurrentPortfolioValue();
        portfolioBalance.setText(new DecimalFormat("#.###").format(currentPortfolioValueFiat) +
                                " " +
                                getCurrencySymbol(currency));
        // switch balance from FIAT to BTC and vice versa
        portfolioBalance.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switchPortfolioCurrency();
                editor.putString("currency", currency);
                editor.apply();
                onResume();
            }
        });

        // gain FIAT/Percentage //
        TextView portfolioGain = findViewById(R.id.portfolioGain);
        float currentPortfolioGain = getPortfolioGain(currentPortfolioValueFiat);

        String sign = "";
        if (currentPortfolioGain >= 0) {
            portfolioGain.setTextColor(getResources().getColor(R.color.green));
            sign = "+";
        } else {
            portfolioGain.setTextColor(getResources().getColor(R.color.red));
        }

        portfolioGain.setText(sign +
                              new DecimalFormat("#.###").format(getPortfolioGainFiat(currentPortfolioValueFiat)) +
                              " " +
                              getCurrencySymbol(currency) +
                              " (" +
                              sign +
                              new DecimalFormat("#.##").format(currentPortfolioGain) +
                              "%)" );
        // switch balance from FIAT to BTC and vice versa
        portfolioGain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switchPortfolioCurrency();
                onResume();
            }
        });

        // portfolio period selector //
        final TextView portfolioPeriod = findViewById(R.id.portfolioPeriod);

        if (portfolioPeriodIsDay == true) {
            String text = "<font color='#777777'>" +
                    getResources().getString(R.string.all_time) +
                    "</font> <font color='#777777'>◀</font> <font color='#f18400'>" +
                    getResources().getString(R.string.today) +
                    "</font>";
            portfolioPeriod.setText(Html.fromHtml(text));
        } else {
            String text = "<font color='#f18400'>" +
                    getResources().getString(R.string.all_time) +
                    "</font> <font color='#777777'>▶</font> <font color='#777777'>" +
                    getResources().getString(R.string.today) +
                    "</font>";
            portfolioPeriod.setText(Html.fromHtml(text));
        }

        portfolioPeriod.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (portfolioPeriodIsDay == true) {
                    portfolioPeriodIsDay = false;
                    editor.putBoolean("portfolioPeriodIsDay", false);
                } else {
                    portfolioPeriodIsDay = true;
                    editor.putBoolean("portfolioPeriodIsDay", true);
                }

                editor.apply();

                onResume();
            }
        });


        // display altcoin in portfolio //
        ListView listview = findViewById(R.id.portfolioItems);

        if (listview != null) {
            PortfolioAdapter adapter = new PortfolioAdapter(this, portfolioItems);
            listview.setAdapter(adapter);

            // Open WebView and display graph
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    assert connectivityManager != null;
                    if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                            connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

                        String altcoinName = portfolioItems.get(position).altcoinName;

                        if (!coin.coinsLabelGraph.get(altcoinName).equals("na")) {
                            loadGraph(altcoinName);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.graphUnavailable, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.networkUnavailable, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            // Edit Portfolio item
            listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    // Change background color on longpress //
                    //ImageView img = findViewById(R.id.portfolioItemBackground);
                    //img.setImageResource(R.color.colorPrimaryDark);

                    String altcoinName = portfolioItems.get(position).altcoinName;
                    float amountBought = portfolioItems.get(position).amountBought;
                    float unitValue = portfolioItems.get(position).unitValue;
                    String altcoinCurrency = portfolioItems.get(position).currency;

                    Intent intent = new Intent(MainActivity.this, EditPortfolioItemActivity.class);
                    Bundle b = new Bundle();
                    b.putString("altcoinDescription", altcoinName);
                    b.putFloat("amountBought", amountBought);
                    b.putFloat("unitValue", unitValue);
                    b.putString("altcoinCurrency", altcoinCurrency);
                    intent.putExtras(b);
                    startActivity(intent);

                    return true;
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (id == R.id.action_addNew) {
            startActivity(new Intent(this, AddNewPortfolioItemActivity.class));
            return true;
        } else if (id == R.id.action_reload) {
            startActivity(new Intent(this, LoadingActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Open the DisplayGraphActivity when menu item is clicked //
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        final ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        assert connectivityManager != null;
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {

            String description = (String) item.getTitle();
            String altcoinName = coin.coinsDescriptionLabelHashtable.get(description);
            loadGraph(altcoinName);

            return true;
        } else {
            Toast.makeText(getApplicationContext(), R.string.networkUnavailable, Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void switchPortfolioCurrency() {
        String currencySet = prefs.getString("currency", "EUR");

        switch (currencySet) {
            case "EUR":
                switch (currency) {
                    case "EUR":
                        currency = "USD";
                        break;
                    case "USD":
                        currency = "BTC";
                        break;
                    case "BTC":
                        currency = "EUR";
                        break;
                }
                break;
            case "USD":
                switch (currency) {
                    case "EUR":
                        currency = "USD";
                        break;
                    case "USD":
                        currency = "BTC";
                        break;
                    case "BTC":
                        currency = "EUR";
                        break;
                }
                break;
            case "BTC":
                switch (currency) {
                    case "EUR":
                        currency = "USD";
                        break;
                    case "USD":
                        currency = "BTC";
                        break;
                    case "BTC":
                        currency = "EUR";
                        break;
                }
                break;
        }
    }

    private void loadGraph(String altcoinName) {
        GraphUrl graphUrl = new GraphUrl(getApplicationContext());
        String url = graphUrl.getUrl(altcoinName);

        Intent intent = new Intent(MainActivity.this, DisplayGraphActivity.class);
        Bundle b = new Bundle();
        b.putString("url", url);
        intent.putExtras(b);
        startActivity(intent);
    }

    private String getCurrencySymbol(String currency) {
        String currencySymbol = null;

        switch (currency) {
            case "EUR":
                currencySymbol = "€";
                break;
            case "USD":
                currencySymbol = "$";
                break;
            case "BTC":
                if (Build.VERSION.SDK_INT >= 26) {
                    currencySymbol = "\u20BF";
                } else {
                    currencySymbol = "BTC";
                }
                break;
        }

        return currencySymbol;
    }

    // Load portfolio items array //
    private void getPortfolioData() {
        String coinName;
        float amountBought;
        float unitValue;
        //float currentUnitValue;
        float lastDayUnitValue;
        String currency;

        int y = 0;

        for (int i = 0; i < coin.coins.length; i ++) {
            amountBought = prefs.getFloat(coin.coins[i] + "_a", -1f);

            if (amountBought != -1f) {
                coinName = coin.coins[i];
                unitValue = prefs.getFloat(coin.coins[i] + "_p", -1f);
                lastDayUnitValue = prefs.getFloat(coin.coins[i] + "_lastDayUnitValue", -1f);
                currency = prefs.getString(coin.coins[i] + "_currency", null);

                portfolioItems.add(y, new PortfolioItem(coinName,
                        amountBought,
                        unitValue,
                        lastDayUnitValue,
                        currency));

                y ++;
            }
        }
    }

    private float getCurrentPortfolioValue() {
        float amountBought;
        float currentCoinValue;
        float total = 0f;

        for (int i = 0; i < coin.coins.length; i ++) {
            amountBought = prefs.getFloat(coin.coins[i] + "_a", 0f);

            if (amountBought > 0) {
                String altcoinCurrency = prefs.getString(coin.coins[i] + "_currency", "EUR");
                currentCoinValue = prefs.getFloat(coin.coins[i] + "_currentUnitValue", 0f);

                if (!altcoinCurrency.equals(currency)) {
                    currentCoinValue = coin.currencyToCurrency(currentCoinValue, altcoinCurrency, currency);
                }

                total += amountBought * currentCoinValue;
            }
        }

        return total;
    }

    private float getPortfolioGainFiat(float currentPortfolioValue) {
        float initialValue = 0f;

        for (int i = 0; i < coin.coins.length; i ++) {
            float amountBought = prefs.getFloat(coin.coins[i] + "_a", 0);

            if (amountBought > 0) {
                //float unitPrice = prefs.getFloat(coin.coins[i] + "_p", 0);
                float unitPrice;

                if (portfolioPeriodIsDay == true) {
                    unitPrice = prefs.getFloat(coin.coins[i] + "_lastDayUnitValue", 0);
                } else {
                    unitPrice = prefs.getFloat(coin.coins[i] + "_p", 0);
                }

                String altcoinCurrency = prefs.getString(coin.coins[i] + "_currency", "EUR");

                if (!altcoinCurrency.equals(currency)) {
                    unitPrice = coin.currencyToCurrency(unitPrice, altcoinCurrency, currency);
                }
                initialValue += amountBought * unitPrice;
            }
        }

        return currentPortfolioValue - initialValue;
    }

    private float getPortfolioGain(float currentPortfolioValue) {
        float initialValue = 0f;

        for (int i = 0; i < coin.coins.length; i ++) {
            float amountBought = prefs.getFloat(coin.coins[i] + "_a", 0);

            if (amountBought > 0) {
                //float unitPrice = prefs.getFloat(coin.coins[i] + "_p", 0);
                float unitPrice;

                if (portfolioPeriodIsDay == true) {
                    unitPrice = prefs.getFloat(coin.coins[i] + "_lastDayUnitValue", 0);
                } else {
                    unitPrice = prefs.getFloat(coin.coins[i] + "_p", 0);
                }

                String altcoinCurrency = prefs.getString(coin.coins[i] + "_currency", "EUR");

                if (!altcoinCurrency.equals(currency)) {
                    unitPrice = coin.currencyToCurrency(unitPrice, altcoinCurrency, currency);
                }

                initialValue += amountBought * unitPrice;
            }
        }

        float gain = (currentPortfolioValue - initialValue) / initialValue * 100;

        // Check that gain is a valid number //
        if (gain != gain) {
            return 0f;
        } else {
            return gain;
        }
    }

    // Display portfolio items list //
    class PortfolioItem {
        final String altcoinName;
        final float amountBought;
        final float unitValue;
        final float lastDayUnitValue;
        final String currency;

        PortfolioItem(String altcoinName, float amountBought, float unitValue, float lastDayUnitValue, String currency) {
            this.altcoinName = altcoinName;
            this.amountBought = amountBought;
            this.unitValue = unitValue;
            this.lastDayUnitValue = lastDayUnitValue;
            this.currency = currency;
        }
    }

    class PortfolioAdapter extends ArrayAdapter<PortfolioItem> {
        PortfolioAdapter(Context context, ArrayList<PortfolioItem> items) {
            super(context, R.layout.single_element_of_portfolio, items);
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.single_element_of_portfolio, null);
            }

            PortfolioItem item = super.getItem(position);

            // Check which currency was used when altcoin was added the first time //
            assert item != null;
            String currencySymbol = getCurrencySymbol(item.currency);

            // Get Current Altcoin value //
            float currentCoinValue = prefs.getFloat(item.altcoinName + "_currentUnitValue",-1f);

            // Portfolio Altcoin Icon //
            ImageView imageView = convertView.findViewById(R.id.portfolioAltcoinLogo);

            if (coin.isItIconomiDaa(item.altcoinName)) {
                int id = getResources().getIdentifier("eu.uwot.fabio.altcoinprices:drawable/" +
                        "ic_menu_icn", null, null);
                imageView.setImageResource(id);
            } else {
                String imageResource = "ic_menu_" + item.altcoinName.toLowerCase();
                int id = getResources().getIdentifier("eu.uwot.fabio.altcoinprices:drawable/" +
                        imageResource, null, null);
                imageView.setImageResource(id);
            }

            // Portfolio Altcoin Name //
            TextView portfolioAltcoinName = convertView.findViewById(R.id.portfolioAltcoinName);

            if (portfolioAltcoinName != null) {
                portfolioAltcoinName.setText(coin.coinsLabelDescriptionHashtable.get(item.altcoinName));
            }

            // Portfolio Altcoin Balance //
            float altcoinBalance = currentCoinValue * item.amountBought;
            TextView portfolioAltcoinBalance = convertView.findViewById(R.id.portfolioAltcoinBalance);

            if (portfolioAltcoinBalance != null) {
                portfolioAltcoinBalance.setText(item.amountBought +
                        " " +
                        //coin.coinsLabelCodeHashtable.get(item.altcoinName) +
                        item.altcoinName +
                        " (" +
                        new DecimalFormat("#.###").format(altcoinBalance) +
                        " " +
                        currencySymbol +
                        ")");

                portfolioAltcoinBalance.setTextColor(getResources().getColor(R.color.colorTextMain));
            }

            // Portfolio Altcoin Gain //
            float investmentValue = 0f;
            float gain = 0f;

            if (portfolioPeriodIsDay == true) {
                if (altcoinBalance > 0f) {
                    investmentValue = item.lastDayUnitValue * item.amountBought;
                    gain = (altcoinBalance - investmentValue) / investmentValue * 100;
                }
            } else {
                if (altcoinBalance > 0f) {
                    investmentValue = item.unitValue * item.amountBought;
                    gain = (altcoinBalance - investmentValue) / investmentValue * 100;
                }
            }

            float gainFiat = altcoinBalance - investmentValue;

            TextView portfolioAltcoinGain = convertView.findViewById(R.id.portfolioAltcoinGain);
            if (portfolioAltcoinGain != null) {
                String sign = "";
                if (gainFiat >= 0f) {
                    sign = "+";
                    //portfolioAltcoinGain.setTextColor(0xff99cc00);
                    portfolioAltcoinGain.setTextColor(getResources().getColor(R.color.green));
                } else {
                    portfolioAltcoinGain.setTextColor(getResources().getColor(R.color.red));
                }

                portfolioAltcoinGain.setText(sign +
                        new DecimalFormat("#.###").format(gainFiat) +
                        " " +
                        currencySymbol +
                        " (" +
                        sign +
                        new DecimalFormat("#.##").format(gain) +
                        "%)");
            }

            // Portfolio Altcoin Unit Value //
            TextView portfolioAltcoinUnitValue = convertView.findViewById(R.id.portfolioAltcoinUnitValue);
            if (portfolioAltcoinUnitValue != null) {
                portfolioAltcoinUnitValue.setText("1" + item.altcoinName +
                        " = " +
                        new DecimalFormat("#.######").format(currentCoinValue) +
                        " " +
                        currencySymbol);
                portfolioAltcoinUnitValue.setTextColor(getResources().getColor(R.color.colorTextMain));
            }
            return convertView;
        }
    }

}
