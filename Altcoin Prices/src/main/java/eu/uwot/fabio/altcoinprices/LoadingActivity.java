package eu.uwot.fabio.altcoinprices;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

public class LoadingActivity extends AppCompatActivity {

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        progressBar = findViewById(R.id.progressBar);

        Thread retrieveData = new Thread() {
            public void run() {
                setPortfolioData();

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        };

        retrieveData.start();
    }

    private void setPortfolioData () {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Settings", 0);
        SharedPreferences.Editor editor = prefs.edit();
        Coin coin = new Coin(getApplicationContext(), false);

        int portfolioItems = 0;
        for (int i = 0; i < coin.coins.length; i ++) {
            float amountBought = prefs.getFloat(coin.coins[i] + "_a", -1f);

            if (amountBought != -1f) {
                portfolioItems += 1;
            }
        }

        int progressBarSlotTime = 100 / (portfolioItems + 4);
        int progressBarProgess = 0;

        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btcusd = coin.getCoinValue("BTC", "USD", false);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        float btceur = coin.getCoinValue("BTC", "EUR", false);
        progressBar.setProgress(progressBarProgess += progressBarSlotTime);
        editor.putFloat("btcusd", btcusd);
        editor.putFloat("btceur", btceur);
        editor.apply();

        progressBar.setProgress(progressBarProgess += progressBarSlotTime);

        for (int i = 0; i < coin.coins.length; i ++) {
            float amountBought = prefs.getFloat(coin.coins[i] + "_a", -1f);

            if (amountBought != -1f) {
                String altcoinCurrency = prefs.getString(coin.coins[i] + "_currency", "EUR");
                float currentUnitValue = coin.getCoinValue(coin.coins[i], altcoinCurrency, false);
                editor.putFloat(coin.coins[i] + "_currentUnitValue", currentUnitValue);

                float lastDayUnitValue = coin.getCoinValue(coin.coins[i], altcoinCurrency, true);
                editor.putFloat(coin.coins[i] + "_lastDayUnitValue", lastDayUnitValue);
                editor.commit();

                progressBarProgess += progressBarSlotTime;
                progressBar.setProgress(progressBarProgess);

                /*Log.d("LOADING", "coin.coins[i]: " + coin.coins[i]);
                Log.d("LOADING", "amountBought: " + amountBought);
                Log.d("LOADING", "altcoinCurrency: " + altcoinCurrency);
                Log.d("LOADING", "currentUnitValue: " + currentUnitValue);*/
            }
        }

        /*float res = coin.getCoinValue("ONT", "USD");
        Log.d("LOADING", "res: " + res);*/

        //String timezone = prefs.getString("timezone", "0");
        //Log.d("LOADING", "timezone: " + timezone);
    }

}
